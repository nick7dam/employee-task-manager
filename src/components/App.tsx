import React from 'react';
import '../styles/style.css';
import '../styles/calendar.css';
import '../styles/statuses.css';
import '../styles/external.css';
import TableComponent from "./table/table";

class App extends React.Component {
    render() {
        return (
            <div>
                <TableComponent></TableComponent>
            </div>
        );
    }
}

export default App;
