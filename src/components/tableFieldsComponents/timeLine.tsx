import React from 'react';
import moment from 'moment';
import CalendarComponent from "../calendar/calendar";

interface IProps {
    value: any
}
interface IState {
    val: any,
    editMode: boolean,
    showFromCalendar: boolean,
    showToCalendar: boolean,
    dateFromError: string,
    dateToError: string,
}

export default class TimeLineComponent extends React.Component<IProps, IState> {
    constructor(props: IProps) {
        super(props);
        this.state = {
            val: props.value,
            editMode: false,
            showFromCalendar: false,
            showToCalendar: false,
            dateFromError: '',
            dateToError: '',
        };

    }

    getTimeLineDates() {
        let fromMonth, toDate, toMonth, fromDate;
        if (this.state.val.dateFrom !== '') {
            fromMonth = moment(this.state.val.dateFrom).format('MMM');
            fromDate = moment(this.state.val.dateFrom).format('DD');
        }
        if (this.state.val.dateTo !== '') {
            toMonth = moment(this.state.val.dateTo).format('MMM');
            toDate = moment(this.state.val.dateTo).format('DD');
        }
        if (!fromMonth && !toMonth) {
            return '-';
        } else if (fromMonth && !toMonth) {
            return fromMonth + ' ' + fromDate + ' - '
        } else if (!fromMonth && toMonth) {
            return ' - ' + toMonth + ' ' + toDate;
        } else if (fromMonth === toMonth) {
            return fromMonth + ' ' + fromDate + ' - ' + toDate;
        } else {
            return fromMonth + ' ' + fromDate + ' - ' + toMonth + ' ' + toDate;
        }
    }

    changeDateFromValue(value: any) {
        let val = this.state.val;
        if (moment(val.dateFrom).isAfter(val.dateTo)) {
            this.setState({
                dateToError: 'From Date cannot be after To Date'
            });
            return false;
        } else {
            val.dateFrom = moment(value).format('YYYY-MM-DD');
            this.setState({
                val: val,
                dateFromError: ''
            });
        }
    }

    changeDateToValue(value: any) {
        let val = this.state.val;
        if (moment(val.dateTo).isBefore(val.dateFrom)) {
            this.setState({
                dateToError: 'To Date cannot be before From Date'
            });
            return false;
        } else {
            val.dateTo = moment(value).format('YYYY-MM-DD');
            this.setState({
                dateToError: '',
                val: val
            });
        }
    }

    clearValue() {
        let newVal = {
            dateFrom: '',
            dateTo: '',
        };
        this.setState({val: newVal,});
    }

    openFromCalendar() {
        this.setState({
            showFromCalendar: true,
            showToCalendar: false,
        });
    }

    openToCalendar() {
        this.setState({
            showFromCalendar: false,
            showToCalendar: true,
        });
    }

    closeFromCalendar() {
        this.setState({
            showFromCalendar: false
        });
    }

    closeToCalendar() {
        this.setState({
            showToCalendar: false
        });
    }

    render() {
        let content = this.getTimeLineDates();
        let daysDiff = moment(this.state.val.dateTo).diff(this.state.val.dateFrom, 'days').toString();
        if (!daysDiff) {
            daysDiff = '';
        } else {
            daysDiff = daysDiff.toString() + 'd';
        }
        let totalProgress = '';
        let hover = '';
        let progress = 0;
        let daysDiffProgress = moment(new Date()).diff(this.state.val.dateFrom, 'days');
        if (daysDiffProgress >= 0) {
            progress = (daysDiffProgress / parseInt(daysDiff)) * 100;
            totalProgress = "linear-gradient(to right, rgb(87, 155, 252) " + progress + "%, rgb(28, 31, 59) " + progress + "%)";
            hover = "linear-gradient(to right, rgb(37, 125, 251) " + progress + "%, rgb(12, 13, 24) " + progress + "%)";
        }
        return (
            <div>
                <div className={"cell-component timerange-cell"} style={{maxWidth: "160px"}}>
                    <div className={"timeline-cell-component "}>
                        <div className={"timeline-bar"} style={{background: totalProgress}}>
                            <div className={"timeline-bar-hover-bg"} style={{background: hover}}></div>
                            <div className={"timeline-picker-button-component"}>
                                <span className={"fa fa-angle-left IGNORE_OPEN_TIMELINE_CLASS"} onClick={() => this.openFromCalendar()}></span>
                            </div>
                            {
                                //@ts-ignore
                            <span className={"timeline-value"} contents={content} hovercontents={daysDiff}></span>
                            }
                            <div className={"timeline-picker-button-component"}>
                                <span className="fa fa-angle-right IGNORE_OPEN_TIMELINE_CLASS" onClick={() => this.openToCalendar()}></span>
                            </div>
                        </div>
                    </div>
                    <i className={"fas fa-times-circle clear-btn"} onClick={() => this.clearValue()}></i>
                </div>
                {
                    this.state.showFromCalendar ? <div className={'animated fadeIn'}>
                        <CalendarComponent clickedOutside={(e: any) => this.closeFromCalendar()} value={this.state.val.dateFrom} changeValue={(e: any) => this.changeDateFromValue(e)}></CalendarComponent>
                        <span className={'small text-danger'}>{this.state.dateFromError}</span>
                    </div> : this.state.showToCalendar ? <div className={'animated fadeIn'}>
                        <CalendarComponent clickedOutside={(e: any) => this.closeToCalendar()} value={this.state.val.dateTo} changeValue={(e: any) => this.changeDateToValue(e)}></CalendarComponent>
                        <span className={'small text-danger'}>{this.state.dateToError}</span>
                    </div> : ''
                }
            </div>
        )
    }
}
