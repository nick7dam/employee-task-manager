import React from 'react';
import { allColorCombinations } from '../../config/allColors';
interface IProps {

}
interface IState {
    val: string,
    editMode: boolean,
    nodeRef: any,
}
export default class ColorPickerComponent extends React.Component<IProps, IState> {
  public allColors: any[];
  constructor(props:any) {
    super(props);
    this.state = {
      val: props.value ? props.value : '',
      editMode: false,
      nodeRef: React.createRef()
    };
    this.allColors = [];
  }
  componentDidMount(){
    this.allColors = this.chunk(allColorCombinations, 15);
    document.removeEventListener('mousedown', this.handleMouseDown.bind(this), false)
    document.addEventListener('mousedown', this.handleMouseDown.bind(this), false)
  }
  componentWillUnmount(){
    document.removeEventListener('mousedown', this.handleMouseDown.bind(this), false)
  }
  handleMouseDown(e:any){
    if(this.state.nodeRef.current === null){
      return;
    }
    if(this.state.nodeRef.current.contains(e.target)){
      return;
    }
    this.handleClickOutside()
  }
  handleClickOutside(){
    this.closeEditMode();
  }
  editComponent(){
    this.setState({editMode: true})
  }
  clearValue(){
    this.setState({val: ''})
    this.save();
  }
  changeValue(e:any, color:string){
    this.setState({val: color})
    this.closeEditMode();
  }
  closeEditMode(){
    this.setState({editMode: false});
  }
  save(){
    this.closeEditMode();
  }
  chunk(array:any, size: number) {
     var arrayOfArrays = [];
    for (var i=0; i<array.length; i+=size) {
      arrayOfArrays.push(array.slice(i,i+size));
    }
    return arrayOfArrays;
  }
  render(){
    let colorPallete = [];
    for(let i = 0; i < this.allColors.length; i++){
      let colorField = []
      for(let j = 0; j < this.allColors[i].length; j++){
        colorField.push(<td onClick={(e)=> this.changeValue(e, this.allColors[i][j])} key={j} style={{backgroundColor: this.allColors[i][j], width: '16px', height: '16px'}}>
        </td>)
      }
      colorPallete.push(<tr key={i}>{colorField}</tr>);
    }
    return (
    <div>
      <div className={"cell-component color-picker-cell suggest-editing"} style={{height: '100%'}} onClick={()=>this.editComponent()}>
        <div className={"color-picker-cell-component"}>
          <div className={"color-indication-component"} style={{backgroundColor: this.state.val}}></div>
            <div className={"ds-text-component color-code-component"} dir="auto">
              <span>{this.state.val}</span>
            </div>
        </div>
        <i className={"fas fa-times-circle clear-btn"} onClick={(e)=> this.clearValue()}></i>
      </div>
      {
        this.state.editMode ? <table ref={this.state.nodeRef}><tbody>{colorPallete}</tbody></table> : ''
      }
    </div>
    )
  }
}
