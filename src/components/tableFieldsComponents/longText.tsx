import React from 'react';

interface IProps {
    value: any
}

interface IState {
    val: string,
    editMode: boolean,
    refName: any
}

export default class LongTextComponent extends React.Component<IProps, IState> {
    constructor(props: IProps) {
        super(props);
        this.state = {
            val: props.value ? props.value : '',
            editMode: false,
            refName: React.createRef()
        };
    }

    editElement(e: any) {
        let self = this;
        this.setState({editMode: true});
        setTimeout(function () {
            self.state.refName.current.focus();
        }, 50)
    }

    checkSave(e: any) {
        if (e.key === 'Enter') {
            this.save(e);
        }
    }

    save(e?: any) {
        this.setState({editMode: false})
    }

    clearValue() {
        this.setState({val: ''});
        this.save();
    }

    render() {
        return (
            <div>
                {
                    !this.state.editMode ?
                        <div className={"cell-component long-text-cell suggest-editing"} onClick={(e) => {
                            this.editElement(e)
                        }}>
                            <div className={"long-text-cell-component"}>
                                <div className={"long-text-cell-value"} dir="auto">
          <span className={"text-content"}>
            {this.state.val}
          </span>
                                </div>
                            </div>
                            <i className="fas fa-times-circle clear-btn" onClick={(e) => this.clearValue()}></i>
                        </div> : <div className={"dialog-node"} style={{overflow: 'inherit'}}>
                            <div className={"ds-dialog-content-wrapper"} style={{position: "relative", left: '0px', top: '0px'}}>
                                <div className={"ds-dialog-content-component bottom opacity-and-slide-enter-done"}>
                                    <div className={"dialog-with-keyboard-wrapper"} style={{marginBottom: "100px"}}>
                                        <div className={"long-text-editor-dialog-wrapper"} style={{width: "100%"}}>
                                            <div className={"long-text-editor-dialog"} style={{width: "250px"}}>
                                                <div className={"long-text-editor-dialog-content"}>
                                                    <textarea onBlur={(e) => this.save(e)} onKeyDown={(e) => this.checkSave(e)} value={this.state.val} onChange={(e) => this.setState({val: e.currentTarget.value})} ref={this.state.refName} maxLength={2000} dir="auto" className={"long-text-editor-component editable"}></textarea>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                }
            </div>
        )
    }
}
