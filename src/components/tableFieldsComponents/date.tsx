import React from 'react';
import moment, {Moment} from 'moment'
import CalendarComponent from "../calendar/calendar";

interface IProps {
 value: any
}

interface IState {
    val: Moment | string,
    editMode: boolean
}

export default class DateComponent extends React.Component<IProps, IState> {
    constructor(props: IProps) {
        super(props);
        this.state = {
            val: moment(props.value).format('YYYY-MM-DD'),
            editMode: false,
        };
        this.changeValue = this.changeValue.bind(this)


    }

    toggleEditComponent(e: any) {
        let edit = this.state.editMode;
        this.setState({editMode: !edit});
    }

    changeValue(value: any) {
        this.setState({
            val: value
        })
    }

    closeEdit() {
        this.setState({
            editMode: false
        })
    }

    clearValue() {
        this.setState({
            val: ''
        })
    }

    render() {
        return (
            <div>
                <div className={"cell-component date-cell suggest-editing"} onClick={(e) => this.toggleEditComponent(e)}>
                    <div className={'date-cell-component justify-content-center suggest-editing'}>
                        <div className={'date-cell-content date-cell-icons-picker-ignore-hide'}>
                            <div key={0} className={"ds-text-component text-center"} dir="center">{this.state.val ? moment(this.state.val).format('MMM DD') : '-'}</div>
                        </div>
                    </div>
                    <i className={"fas fa-times-circle clear-btn"} onClick={(e) => this.clearValue()}></i>
                </div>
                {this.state.editMode ? <div className={"dialog-node"}>
                    <div className={'animated fadeIn'} style={{paddingTop: '50px'}}>
                        <CalendarComponent clickedOutside={() => this.closeEdit()} value={this.state.val} changeValue={this.changeValue}></CalendarComponent>
                    </div>
                </div> : ''
                }
            </div>
        )
    }
}
