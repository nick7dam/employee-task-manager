import React from 'react';
import moment from "moment";
interface IProps {
    changeValue: any,
    value: any,
    clickedOutside: any
}
interface IState {
    val?: Date,
    label?: string,
    calendar?: any,
    weekDaysData?: any[],
    daysData?: any[],
    nodeRef?: any,
}
export default class CalendarComponent extends React.Component<IProps, IState> {
    public months: any[];
    public weekDays: any[];
    public selectedDate: any;
    public selectedMonth: any;
    public selectedYear: any;
    constructor(props:any) {
        super(props);
        this.state = {
            val: props.value ? props.value : new Date(),
            label: '',
            calendar: {},
            weekDaysData:[],
            daysData:[],
            nodeRef: React.createRef()
        };
        this.months = ["January", "February", "March", "April", "May", "June", "July","August", "September", "October", "November", "December"],
            this.weekDays = [{
                name: 'Monday',
                shortName: 'Mon'
            },{
                name: 'Tuesday',
                shortName: 'Tue'
            },{
                name: 'Wednesday',
                shortName: 'Wed'
            },{
                name: 'Thursday',
                shortName: 'Thu'
            },{
                name: 'Friday',
                shortName: 'Fri'
            },{
                name: 'Saturday',
                shortName: 'Sat'
            },{
                name: 'Sunday',
                shortName: 'Sun'
            }];
        this.selectedDate = new Date(props.value).getDate();
        this.selectedMonth = null;
        this.selectedYear = null;
    }
    componentDidMount(){
        document.removeEventListener('mousedown', this.handleMouseDown.bind(this), false)
        document.addEventListener('mousedown', this.handleMouseDown.bind(this), false)
        let weekDays = [];
        for(let i=0; i < this.weekDays.length; i++){
            weekDays.push(<td key={i}>{this.weekDays[i].shortName}</td>);
        }
        this.selectedMonth = moment(this.state.val).month();
        this.selectedYear = moment(this.state.val).year();
        this.setState({
            weekDaysData: weekDays,
            daysData: this.createCal(this.selectedYear, this.selectedMonth)
        });
    }
    componentWillUnmount(){
        document.removeEventListener('mousedown', this.handleMouseDown.bind(this), false)
    }
    handleMouseDown(e:any){
        if(this.state.nodeRef.current === null){
            return;
        }
        if(this.state.nodeRef.current.contains(e.target)){
            return;
        }
        this.handleClickOutside()
    }
    handleClickOutside(){
        this.props.clickedOutside()
    }
    createCal(year:any, month:any) {
        let todayDate = new Date().getDate();
        let todayMonth = new Date().getMonth();
        let todayYear = new Date().getFullYear();
        let day = 1,
            i,
            j,
            haveDays = true,
            startDay = new Date(year, month, day).getDay(),
            daysInMonth = [31, (((year%4===0)&&(year%100!==0))||(year%400===0)) ? 29 : 28, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31 ],
            calendar = new Array();
        i = 0;
        if(startDay === 0){
            startDay = 6;
        } else {
            startDay -= 1;
        }
        while(haveDays) {
            calendar[i] = [];
            for (j = 0; j < 7; j++) {
                if(i === 0){
                    if(j >= startDay) {
                        calendar[i][j] = day++
                    }
                }
                else if(day <= daysInMonth[month] && i > 0){
                    calendar[i][j] = day++
                }  else {
                    calendar[i][j] = "";
                    haveDays = false;
                }
                if(day > daysInMonth[month]){
                    haveDays = false;
                }
            }
            i++;
        }
        for (let k = 0; k < calendar.length; k++) {
            let tdEl = [];
            for (let z = 0; z < calendar[k].length; z++) {
                let classesToAdd = '';
                if(parseInt(this.selectedDate) === calendar[k][z]){
                    classesToAdd+=' selected';
                } else if(todayDate === calendar[k][z] && todayMonth === month && todayYear === year){
                    classesToAdd+=' today';
                } else if(parseInt(this.selectedDate) === calendar[k][z]){
                    classesToAdd+=' selected';
                }
                tdEl.push(React.createElement('td', {key: z, className: classesToAdd, onClick: (e) => {this.dateClicked(e)} }, calendar[k][z]));
            }
            calendar[k] = React.createElement('tr', {key: k}, tdEl);
        }
        return calendar;
    }
    dateClicked(e:any){
        let newDate = parseInt(e.currentTarget.textContent);
        this.selectedDate = newDate;
        let weekDays = [];
        for(let i=0; i < this.weekDays.length; i++){
            weekDays.push(<td key={i}>{this.weekDays[i].shortName}</td>);
        }
        let value = moment(new Date(this.selectedYear, this.selectedMonth, this.selectedDate)).format('YYYY-MM-DD');
        let daysData = this.createCal(moment(this.state.val).year(), moment(this.state.val).month());
        this.setState({
            val: new Date(value),
            label: this.state.label,
            calendar: this.state.calendar,
            weekDaysData: weekDays,
            daysData:daysData,
            nodeRef: this.state.nodeRef
        });
        this.props.changeValue(value)
    }
    nextMonth(){
        let newDate = moment(this.state.val).subtract(1, 'month');
        let dateF = new Date(newDate.toString());
        this.selectedDate = null;
        this.selectedMonth = new Date(newDate.toString()).getMonth();
        this.selectedYear = new Date(newDate.toString()).getFullYear();
        let weekDays = [];
        for(let i=0; i < this.weekDays.length; i++){
            weekDays.push(<td key={i}>{this.weekDays[i].shortName}</td>);
        }
        this.setState({
            val: dateF,
            weekDaysData: weekDays,
            daysData: this.createCal(new Date(newDate.toString()).getFullYear(), new Date(newDate.toString()).getMonth())
        });
    }
    prevMonth(){
        let newDate =  moment(this.state.val).add(1, 'month');
        let dateF = new Date(newDate.toString());
        this.selectedDate = null;
        let weekDays = [];
        this.selectedMonth = new Date(newDate.toString()).getMonth();
        this.selectedYear = new Date(newDate.toString()).getFullYear();
        for(let i=0; i < this.weekDays.length; i++){
            weekDays.push(<td key={i}>{this.weekDays[i].shortName}</td>);
        }
        this.setState({
            val: dateF,
            weekDaysData: weekDays,
            daysData: this.createCal(new Date(newDate.toString()).getFullYear(), new Date(newDate.toString()).getMonth())
        });
    }
    render() {
        return (
            <div ref={this.state.nodeRef}>
                <div id="cal">
                    <div className={"header"}>
                        <span className={"left button"} id="prev" onClick={(e)=>{this.nextMonth()}}> &lang; </span>
                        <span className={"left hook"}></span>
                        <span className={"month-year"} id="label"> {moment(this.state.val).format('MMM')} {moment(this.state.val).format('YYYY')} </span>
                        <span className={"right hook"}></span>
                        <span className={"right button"} id="next" onClick={(e)=>{this.prevMonth()}}> &rang; </span>
                    </div>
                    <table id="days">
                        <tbody>
                        <tr>
                            {this.state.weekDaysData}
                        </tr>
                        </tbody>
                    </table>
                    <div id="cal-frame">
                        <table id="cal-frame">
                            <tbody>
                            {this.state.daysData}
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        )
    }
}
